FROM alpine:3.11

ENV RUNTIME_LIBS="ca-certificates curl gettext bash jq"
ARG KUBECTL_VERSION=1.15.0

RUN apk add --no-cache ${RUNTIME_LIBS}

#Set bash as shell
RUN sed -i -e "s/bin\/ash/bin\/bash/" /etc/passwd
ENV LC_ALL=en_US.UTF-8

RUN curl -LOs https://storage.googleapis.com/kubernetes-release/release/v${KUBECTL_VERSION}/bin/linux/amd64/kubectl && \
    chmod +x ./kubectl && \ 
    mv ./kubectl /usr/local/bin/kubectl
